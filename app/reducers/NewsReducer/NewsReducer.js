import update from 'immutability-helper'
import {
	REQUEST_FEED_ARTICLES,
	SUCCESS_FETCH_ARTICLES,
	FAILURE_FETCH_ARTICLES,
	REQUEST_ARTICLE,
	FAILURE_FETCH_ARTICLE,SUCCESS_FETCH_ARTICLE,SUCCESS_FETCH_MORE_ARTICLES,REQUEST_MORE_ARTICLES } from '../../constants/NewsConstants/NewsConstants'
import moment from 'moment'
const initialState = {
	loading : false,
	stories: [],
	featured: {
		top:[],
		secundary:[]
	},
	currentPage: undefined
}

const NewsReducer = (nextState = initialState,action) => {

	switch (action.type) {
		case REQUEST_FEED_ARTICLES:
			return update(nextState,{
				loading: {$set: true},
			})
			break;
	
		case REQUEST_MORE_ARTICLES:
			return update(nextState,{
				loading: {$set: true},
			})
			break;

		case SUCCESS_FETCH_ARTICLES:
			return update(nextState,{
				featured : {
					top:{$set: action.payload.featured.top},
					secundary:{$set: action.payload.featured.secundary}
				},
				stories: { $set: action.payload.posts },
				loading: {$set: false },
				currentPage: {$set: action.page }
			})
		break;

		case SUCCESS_FETCH_MORE_ARTICLES:
			return update(nextState,{
				stories: { $push: action.payload.posts },
				loading: {$set: false },
				currentPage: {$set: action.page }
			})
		break;

		default:
			return nextState
			break;
	}
}

export default NewsReducer