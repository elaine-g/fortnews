import { TEST_SUCESS, TEST_FAIL } from '../../constants/TestConstants/Index'

const initialState = {
	tested: false
}

const TestReducer = (nextState = initialState,action) => {
	switch(action.type){

		case TEST_SUCESS:
			return Object.assign({}, nextState, {
				tested: true
			});     
		break

		default:
			return nextState
		break

	}
}

export default TestReducer