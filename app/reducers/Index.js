import { combineReducers } from 'redux'
import TestReducer from './TestReducer/Index'
import NewsReducer from './NewsReducer/NewsReducer'
import PromotionsReducer from './PromotionsReducer/PromotionsReducer'
export default combineReducers({
	TestReducer,
	FeedNews: NewsReducer,
	PromotionsReducer
})