import update from 'immutability-helper'
import {
	REQUEST_PROMOTIONS,
	FAILURE_FETCH_PROMOTIONS,
	SUCESS_FETCH_PROMOTIONS
} from '../../constants/PromotionConstants/PromotionConstants'
import moment from 'moment'
const initialState = {
	requested : false,
	lastUpdated: null,
	loading: false,
	promotions:[]
}

const PromotionsReducer = (nextState = initialState,action) => {

	switch (action.type) {
		case REQUEST_PROMOTIONS:
			return update(nextState,{
				requested:{ $set: true },
				time: {$set:moment().unix() },
				loading: {$set: true },
			})
			break;

		case SUCESS_FETCH_PROMOTIONS:
			return update(nextState,{
				loading: { $set: false},
				lastUpdated:{ $set: Date.now()},
				promotions:{$set: action.payload.promotions}
			})	
		break;

		default:
			return nextState
			break;
	}
}

export default PromotionsReducer