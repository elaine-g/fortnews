import {REQUEST_FEED_ARTICLES} from '../constants/NewsConstants/NewsConstants'

const setTested = () => ({ type: REQUEST_FEED_ARTICLES }) 

const TestActionCreator = {

	updateTested(){
		return(dispatch,getState) => {
			try {
				dispatch(setTested())
				console.log('TestActionCreator.updateTested',getState().TestReducer)
			} catch(e) {
				console.log(e);
			}
			
		}

	}
}

export default TestActionCreator