import { REQUEST_FEED_ARTICLES,SUCCESS_FETCH_ARTICLES ,SUCCESS_FETCH_MORE_ARTICLES,REQUEST_MORE_ARTICLES} from '../constants/NewsConstants/NewsConstants'
import API from '../utilities/api/API'


export const requestTest = () => ({ type: REQUEST_FEED_ARTICLES })

export const requestMoreStories = () => ({ type: REQUEST_MORE_ARTICLES})
export const successRequestStories = (data,page) => ({ type: SUCCESS_FETCH_ARTICLES, payload:data, page: page })
export const successRequestMoreStories = (data,page) => ({ type: SUCCESS_FETCH_MORE_ARTICLES, payload:data, page: page })

export const NewsActionCreator = {
	requestStories(page){
		return(dispatch,getState) => {
			dispatch(requestTest())
			if(getState().FeedNews.loading){
				API.stories.requestStories(page).then((res) => {
					dispatch(successRequestStories(res,page))
				}).catch((err)=>{
					console.log(err)
				})
				// dispatch(successRequestStories(res))
			}
			return false
		}
	},
	requestMoreStories(page){
		return(dispatch,getState) => {
			dispatch(requestMoreStories())
			if(getState().FeedNews.loading){
				API.stories.requestStories(page).then((res) => {
					dispatch(successRequestMoreStories(res,page))
				}).catch((err)=>{
					console.log(err)
				})
				// dispatch(successRequestStories(res))
			}
			return false
		}
	},
}