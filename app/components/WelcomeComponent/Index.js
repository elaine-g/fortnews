import React,{ Component } from 'react'
import { View,Text,Button,AsyncStorage } from 'react-native'
import { FortNowAuth } from '../../utilities/models/fortnow-auth/Index'
export default class WelcomeComponent extends Component{
	constructor(props) {
	  super(props);
	
	  this.state = {};
	  this.__go = this.__go.bind(this)
	  
	}


	__go(route){
		this.props.navigation.navigate(route);
	}

	async componentDidMount(){
		if(await FortNowAuth.auth() === true)
			this.__go('App')
		else
			this.__go('NotAuthenticated')
	}

	render(){
		return(
			<View>
				<Text>
					Welcome Component!
				</Text>
			</View>
			)
	}
}
