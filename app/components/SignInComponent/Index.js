import React,{ Component } from 'react'
import { View,Text,Button } from 'react-native'
import {HeaderBackButton} from "react-navigation"

export default class SignInComponent extends Component{
	static navigationOptions = ({ navigation }) => {
		return{
			title: 'Iniciar sesión',
			headerTitleStyle: {
		      flex: 1,
		      textAlign: 'center',
		      alignSelf: 'center',
		      fontWeight: 'normal',
		      fontSize: 14
		    },
		    headerRight: (<View />),
			headerLeft: (
				<HeaderBackButton
				tintColor='#62CF6B' onPress={() => navigation.dispatch({ type: 'Navigation/BACK' }) }
      			/>
		    ),
	    }
	}

	constructor(props) {
	  super(props);
	
	  this.state = {};
	}

	render(){
		return(
			<View>
				<Text>SigInComponent</Text>
			</View>
			)
	}

}