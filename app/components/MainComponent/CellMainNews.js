/**
 * Copyright (c) 2017-present, Liu Jinyong
 * All rights reserved.
 *
 * https://github.com/huanxsd/MeiTuan  
 * @flow
 */

import React, {PureComponent} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image, PixelRatio,Dimensions,ImageBackground} from 'react-native'
import {EvilIcons} from '@expo/vector-icons'
const color = {
    theme: '#06C1AE',
    border: '#e0e0e0',
    background: '#f3f3f3',
    title: '#fff'
}

class CellMainNews extends PureComponent {

    render() {
        console.log('render cell')
       

        this.props.data.imageUrl = this.props.data.imageUrl.replace('w.h', '160.0')
       
        if(this.props.isFirst && this.props.data.type !== 'link' && this.props.isFeatured){
            return(
            <TouchableOpacity style={[featured.container]} onPress={() => console.log('pressed!')}>
                <ImageBackground style={{ width: Dimensions.get('window').width }} source={{uri: this.props.data.imageUrl}} >
                <Text style={[featured.h1,featured.textWithShadow]}>{ this.props.data.title }</Text>
                </ImageBackground>
            </TouchableOpacity>                 
                )
        }

        if(this.props.isSecundary && this.props.data.type !== 'link'){
             return(
            <TouchableOpacity style={[featured.column,{flexDirection: "row",
          justifyContent: "space-between"}]} onPress={() => console.log('pressed!')}>
                <ImageBackground style={{ width: Dimensions.get('window').width / 2 }} source={{uri: this.props.data.imageUrl}} >
                <Text style={[featured.h3,featured.textWithShadow]}>{ this.props.data.title }</Text>
                </ImageBackground>

            </TouchableOpacity>                 
                )           
        }

        // <Image source={{uri: this.props.data.imageUrl}} style={styles.icon} />
        if(this.props.data.type==='link'){
            return(
            <View style={{overflow: 'hidden'}}>
            <TouchableOpacity style={styles.textContainer} onPress={this.props.onPress}>
                

                <View style={styles.rightContainerLink}>
                    <Text style={[styles.h1Link,styles.colorTitle]}>{this.props.data.title}</Text>
                    <View>
                    </View>
                    <Text style={[styles.description]} numberOfLines={0}>{this.props.data.description}</Text>
                    <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={[styles.h1, styles.price]}>{this.props.data.id}元 </Text>
                        <EvilIcons style={{ alignItems : 'flex-end',alignSelf :'flex-end' }} name='external-link' size={50} color='#fff' />
                    </View>

                </View>
            </TouchableOpacity>  
            </View>              
                )
        }
        return (
            <TouchableOpacity style={[styles.container,this.props.isFirst ? styles.first : styles.notfirst]} onPress={this.props.onPress}>
                <Image source={{uri: this.props.data.imageUrl}} style={styles.icon} />

                <View style={styles.rightContainer}>
                    <Text style={styles.h1}>{this.props.data.title}</Text>
                    <View>
                    </View>
                    <Text style={styles.p} numberOfLines={0} style={{marginTop: 8}}>{this.props.data.description}</Text>
                    <View style={{flex: 1, justifyContent: 'flex-end'}}>
                        <Text style={[styles.h1, styles.price]}>{this.props.data.id}元</Text>
                    </View>

                </View>
            </TouchableOpacity>
        )
    }
}
const featured = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        height: 140,
    },
    column:{
        flexDirection: 'row',
        width: Dimensions.get('window').width /2,
        height: 140,    
    },
    h1:{
        alignSelf:'center',
        alignItems:'center',
        fontSize:34,
        fontWeight:'bold',
        color:'#fff',
        paddingTop:14,
        justifyContent: 'center',
    },
    h3:{
        textAlignVertical: 'bottom',
        fontSize:15,
        fontWeight:'300',
        color:'#fff',
    },
    textWithShadow:{
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    }

})
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 10,
        borderBottomWidth: 1 / PixelRatio.get(),
        borderColor: color.border,
        backgroundColor: 'white',
        width: '93.5%',
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 5,
        borderRadius:3
    },
    textContainer:{
        flexDirection: 'row',
        padding: 10,
        borderBottomWidth: 1 / PixelRatio.get(),
        borderColor: color.border,
        backgroundColor: '#DA2175',
        width: '93.5%',
        alignContent: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 5,
        marginBottom: 5,
        borderRadius:3
    },
    icon: {
        width: 80,
        height: 80,
        borderRadius: 5,
        backgroundColor:'#ddd'
    },
    rightContainer: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 10,
    },
    rightContainerLink:{
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5
    },
    price: {
        color: color.theme
    },
    h1: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#222222',
    },
    h1Link:{
        fontSize: 40,
        fontWeight: 'bold',
        textDecorationLine :'underline'
    },
    description:{
        fontSize: 15,
        fontWeight:"100",
        color:'#fff',
        marginTop: 8
    },
    p: {
        fontSize: 13,
        color: '#777777',
    },
    colorTitle:{
        color: color.title
    },
    first:{
        marginTop:15,
        marginBottom:0
    },
    notfirst:{

    }
})

export default CellMainNews