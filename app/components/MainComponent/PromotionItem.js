import React,{ Component } from 'react'
import { View,Text,Buttom,Image,StyleSheet } from 'react-native'

export default class PromotionItem extends Component{
	constructor(props) {
	  super(props);
	
	  this.state = {};
	  this.__renderItem = this.__renderItem.bind(this)
	}

	__renderItem(item,index){
		return(
			<View style={styles.row} key={index}>
				<Image style={{ height:140, width:140 }} source={{uri: 'http://192.168.0.2:3000' + item.source }} />
				<Text>{ item.id }</Text>
			</View>
			)
	}



	render(){
		return(
			<View>
			{
            this.props.promotions && this.props.promotions.map((item,index) => {
            	if(item.status !== false){
            		return(
            		this.__renderItem(item,index)
                	)
           		}else{
           			return (null)
           		}
           	})         
        	}
        	</View>  
			)
	}
}

const styles = StyleSheet.create({
	row:{
		borderBottomWidth: 1,
		borderTopWidth: 1,
		borderColor:'#ddd' 
	}

})