import React, {Component} from 'react';
import {NavigationActions,DrawerActions} from 'react-navigation';
import {ScrollView, Text, View,ToastAndroid,Image,ActivityIndicator} from 'react-native';
import { connect } from 'react-redux'
import { PromotionsActionCreator } from '../../actions/PromotionsActionCreator'

import PromotionItem from './PromotionItem'

class MainComponentDrawer extends Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
    };
  }

  componentDidMount(){
    console.log('componentDrawer mounted')
    this.props.testRequest()
    
  }

  componentWillUnmount(){
    console.log('componentDrawer unmounted')
  }

  navigateToScreen = (route) => () => {

  const navigateAction = NavigationActions.navigate({
    routeName: route
    });
    this.__closeDrawer()
    this.props.navigation.dispatch(navigateAction);
    
  }

  __closeDrawer(){
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  render () {
    return (
      <View>
        <ScrollView>
          <View>

            <View style={{ marginTop:20}}>
              <Text style={{ padding:15 }} onPress={this.navigateToScreen('Updates')}>
              Home
              </Text>
              <Text style={{ padding:15 }} onPress={this.navigateToScreen('Store')}>
              Store
              </Text>
            </View>
          </View>

        </ScrollView>
        {(() => {
          if(this.props.loading){
            return(
              <View><ActivityIndicator color="#D3D8CF" size={25} /></View>
              )
          }else{
            return(
            <PromotionItem promotions={ this.props.promotions } />  
              )
          }
        })()}
        
      </View>
    );
  }
}

const mapStateToProps = (state,props) => {
  return{
  
    time: state.PromotionsReducer.time,
    loading: state.PromotionsReducer.loading,
    promotions: state.PromotionsReducer.promotions
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    testRequest : () => dispatch(PromotionsActionCreator.testRequest(),dispatch),
    dispatch
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(MainComponentDrawer)
