import React,{ Component } from 'react'
import { 
	Text,
	View,
	FlatList,StyleSheet,Dimensions,ScrollView,ActivityIndicator,SectionList } from 'react-native'
import { connect } from 'react-redux'
import CellMainNews from './CellMainNews'
import TestActionCreator from '../../actions/TestActionCreator'
import { NewsActionCreator } from '../../actions/NewsActionCreator'

const overrideRenderItem = ({ item, index, section: { title, data } }) => <CellMainNews isFeatured={true} isFirst={index===0 ? true: false} data={ item } onPress = {item.onPress } />
const overrideRenderItemTwo = ({ item, index, section: { title, data } }) => {
  return (
    <FlatList
      initialNumToRender={2}
      showsHorizontalScrollIndicator={false}
      pagingEnabled={true}
      horizontal={true}
      data={item}
      keyExtractor={(item, index) => index.toString()}
      renderItem={(
        ({item}) => (<CellMainNews isSecundary={true} isFirst={index===0 ? true: false} data={ item } onPress = {item.onPress } />)
      )}
    />
  );
}
class MainComponent extends Component{
	
	static navigationOptions ={
		title: 'Latest',
	}

	constructor(props) {
	  super(props);
	
	  this.state = {
	  	stories:[],
	  	refreshingUpdates : false,
	  	awaitingData: false,
	  	newsPage: 1
	  };
	  this.__renderItem = this.__renderItem.bind(this)
	  this.__refreshFlatUpdates = this.__refreshFlatUpdates.bind(this)
	  this.__getMoreStories = this.__getMoreStories.bind(this)
	  this.__viewDetails = this.__viewDetails.bind(this)
	}

	__getMoreStories(page){
		
		if(!this.props.loading){
			this.props.requestMoreStories(page + 1)
		}

		//this.setState((prevState, props) => {
		//  return {stories: prevState.stories.push(updateStories) };
		//})
	}


	__viewDetails(data){
		console.log('__viewDetails',data.item)
	}

	__renderItem(data){
		return(data) =>{

			return <CellMainNews isFirst={data.index === 0 ? true : false} data={data} onPress={() => this.__viewDetails(data) } />
		}
	}

	__refreshFlatUpdates(){
		if(!this.props.loading)
			this.props.requestStories(1)
	}

	componentDidMount(){
		// calling test action creator
		this.props.requestStories(1)

	}

	componentWillUnmount(){
		console.log('desmontado')
	}
	
	FlatListItemSeparator =()=> {
	    return (
	      <View
	        style={{
			borderWidth: 0,borderColor:'#fff'
	        }}
	      />
	    );
	  }

	render(){

		return(
		<View>
		    <SectionList
		      renderItem={({item, index, section}) => <CellMainNews isFirst={index===0 ? true: false} data={ item } onPress = {item.onPress } />}
		      /*renderSectionHeader={({section: {title}}) => (
		        <Text style={{fontWeight: 'bold'}}>{title}</Text>
		      )}
		      {data: ... title:'',}
		      */
		      sections={[
		        {data: this.props.featured.top, renderItem: overrideRenderItem },
		        {data: [this.props.featured.secundary], renderItem: overrideRenderItemTwo },
		        {data: this.props.stories},

		      ]}
		      onRefresh= { () => this.__refreshFlatUpdates() }
		      refreshing={ this.props.loading } 
		      onEndReached={() => this.__getMoreStories(this.props.currentPage)}
		      onEndReachedThreshold={0.01}
		      keyExtractor={(item, index) => index.toString()}
		        />

		        {this.props.loading &&
		            <View>
		                <ActivityIndicator size={100} color="red" animating={this.props.loading} />
		            </View>
		        }
		</View>

			)
	}
}
/*
	render(){
		return(
			<View>

			<View style={styles.container}>
				<FlatList 
				style={styles.view_container} 
				onRefresh= { () => this.__refreshFlatUpdates() }
				refreshing={ this.props.loading } 
				data={this.props.stories} 
				renderItem={({item,index}) => <CellMainNews isFirst={index===0 ? true: false} data={ item } onPress = {item.onPress } /> }
				onEndReached={() => this.__getMoreStories(this.props.currentPage)}
				onEndReachedThreshold={0.01}
				ItemSeparatorComponent={() => this.FlatListItemSeparator()}
				keyExtractor={(item, index) => String(index)}
				/>	
			</View>
				{this.props.loading &&
				    <View>
				        <ActivityIndicator size={100} color="red" animating={this.props.loading} />
				    </View>
				}
			</View>

			)
	}
*/
const mapStateToProps = (state,props) => {
	return{
		loading : state.FeedNews.loading,
		stories : state.FeedNews.stories,
		featured : state.FeedNews.featured,
		currentPage : state.FeedNews.currentPage 
	}
}


const mapDispatchToProps = (dispatch) =>{
	return{
		requestStories : (page) => dispatch(NewsActionCreator.requestStories(page)),
		requestMoreStories: (page) => dispatch(NewsActionCreator.requestMoreStories(page)),
		dispatch
		}
}


export default connect(mapStateToProps,mapDispatchToProps)(MainComponent)

//50358172 horizon prop flatlist contraseña rappi : RESIDENT, contraseña nip: 3105 contraseña bancO:
const styles = StyleSheet.create({
	view_container:{
		flex:1,
	    height: 100,
	    width: Dimensions.get('window').width,
	    backgroundColor:'#220031'	
	},
	container: {
	    height: Dimensions.get('window').height ,
	    width: Dimensions.get('window').width
	},
	wrapper:{
		flex:1
	},
	footer:{
		flex:1,
		height:20
	},
	row: { padding: 42,backgroundColor:"#8202D5", }
});

