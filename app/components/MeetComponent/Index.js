import React,{ Component } from 'react'
import {NavigationActions} from 'react-navigation'
import { Text,View,FlatList,StyleSheet,Button } from 'react-native'
import moment from 'moment'

export default class MeetComponent extends Component{

	static navigationOptions ={
		title: 'Meet & Play',
	}

	constructor(props) {
	  super(props);
	  this.state = {
	  	loaded: false,
	  	lastUpdate: Date.now(),
	  	lastUpdateString: null,
	  	isFocused: false
	  }
	  this.__timeAgo = this.__timeAgo.bind(this)
	  this.__go = this.__go.bind(this)
	}

	componentWillMount(){
		this.subs = [
			this.props.navigation.addListener("didFocus", this.__handleFocus.bind(this)),
        	this.props.navigation.addListener("willBlur", this.__handleWillBlur.bind(this))
        ];	
  	}

	componentDidMount(){
		console.log('montado MeetComponent')
	}

	componentWillUnmount(){
		console.log('desmontado MeetComponent')
		this.subs.forEach(sub => sub.remove());
	}
	__handleWillBlur(){
	
		this.setState({ 
			isFocused: false,
			lastUpdate: Date.now(),
			lastUpdateString: null,
		});  
			
	}
	__handleFocus(evt){
		
		this.setState({ 
			loaded: true,
			lastUpdate: Date.now(),
			lastUpdateString: moment(this.state.lastUpdate).fromNow(),
			isFocused: true,
		});  
		this.__timeAgo();
	}

	__timeAgo(){
		setInterval(() => {
			this.setState({
				lastUpdateString: moment(this.state.lastUpdate).fromNow()
			})
		},1000 * 30)
	}

	__getFakeAuth(){
		return { sign:'ab-c1059-saaa' };
	}
	__go(route){

		this.props.navigation.navigate(route,{ authData: this.__getFakeAuth() });
	}

	render(){
		return(
			<View >
				<Text style={{ fontSize: 25}}> MEETCOMPONENT { this.state.lastUpdateString } </Text>
				<Button onPress={() => this.__go('Store') } title="ir a la tienda"/>
			</View>
			)
	}
}
