import axios from 'axios'
const API = {
	promotions:{
		requestPromotions(){
			console.log('REQUEST PROMOTIONS CALLED FROM API')
			return axios.get('http://192.168.0.3:3000/promotions/es/get').then((res) => {

				return res.data
			}).catch((err) => {
				return err
			})
		}
	},
	stories:{
		requestStories(page){
			return axios.get('http://192.168.0.3:3000/posts/es/'+page+'/get').then((res) => {
				return res.data
			}).catch((err) => {
				return err
			})
		}
	}
}

module.exports = API