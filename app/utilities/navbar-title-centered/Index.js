export default props => (
  <View style={styles.container}>
    <Text style={[styles.text, props.style]}>{props.children}</Text>
  </View>
);

const styles = getStyles(COLORS => ({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  text: {
    // Text Styling
  },
}));