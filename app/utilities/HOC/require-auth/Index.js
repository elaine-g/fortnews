import React,{Component} from 'react';
import { ScrollView,View,Text,AsyncStorage,Button,Image } from 'react-native';
import { StackActions,NavigationActions } from 'react-navigation';

const withSpinnerxTwo = (PassedComponent) =>
  class withSpinnerxTwo extends Component {
    state = {
      lol: [],
      isAuth: false
    }
    async componentDidMount() {
        const isAuth = await AsyncStorage.getItem('auth')
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'Signin' })],
        });
        if(!isAuth){
            this.setState({
                isAuth: false
            })
        }else {
            this.setState({
                isAuth: true
            })
        }
    }
    render() {
        if(!this.state.isAuth)
            return(
                <View style={{ flex:1, alignContent:'center' }}>
                    <Text style={{ textAlign:'center' }}> Inicia sesión para disfrutar de esta característica </Text>
                    <Button onPress={ () => this.props.navigation.navigate('Signin') } title="Iniciar sesión" />
                    <Image source={{ uri:'http://localhost:3000/statics/promotion.png'}} />
                </View>
                    )
        else
            return (
                <PassedComponent {...this.props}>
                    {this.props.children}
                </PassedComponent>
            )
    }
  }


export default withSpinnerxTwo;