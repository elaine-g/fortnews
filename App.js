import React from 'react';
import { StyleSheet, Text, View,Dimensions } from 'react-native';
import { createStackNavigator,
  createMaterialTopTabNavigator,
  createDrawerNavigator,createSwitchNavigator } from 'react-navigation'

import MainComponent from './app/components/MainComponent/Index'
import MeetComponent from './app/components/MeetComponent/Index'
import StoreComponent from './app/components/StoreComponent/Index'
import WelcomeComponent from './app/components/WelcomeComponent/Index'
import MainComponentDrawer from './app/components/MainComponent/MainComponentDrawer'
import SignInComponent from './app/components/SignInComponent/Index'

import CardStackStyleInterpolator from 'react-navigation/src/views/StackView/StackViewStyleInterpolator'

import { Provider } from 'react-redux'
import store from './app/store/Index'

const MyTabNotAuthenticated = createMaterialTopTabNavigator({
  Updates:{
    screen: MainComponent
  },
  Settings:{
    screen: MeetComponent
  },
},{
  initialRouteName:'Settings',
  order:['Settings','Updates'],

  tabBarOptions:{
    // default elevation 4 220031 #DB5B7F
    style:{ marginTop : 25, backgroundColor:'#DB5B7F', shadowColor:'transparent',elevation:0},
    indicatorStyle:{
      backgroundColor:'transparent',
      width: '25%',
      alignSelf:'center',
      alignContent:'center',
      alignItems:'center',
      flex:1,
      justifyContent:'center',
      left: (Dimensions.get('window').width / 2) / 2 /2,
      marginBottom:10,
      height:30 ,
      borderTopWidth:1,
      borderBottomWidth:1,
      borderRightWidth:1,
      borderLeftWidth:1,
      borderColor:'#fff'
    }
  }
})

const MyStackNotAuthChildren = createStackNavigator({
  Store:{
    screen: StoreComponent
  },
  Signin:{
    screen: SignInComponent
  }
},{
  headerMode: 'screen',

})

const MyStackNotAUth = createStackNavigator({
  Home:{
    screen: MyTabNotAuthenticated
  },
  Others:{
    screen: MyStackNotAuthChildren
  }
},{
  initialRouteName:'Home',
  headerMode: 'none',
  transitionConfig: () => ({
        screenInterpolator: CardStackStyleInterpolator.forHorizontal,
   }),
})

const RootAppNotAuth = createDrawerNavigator({
  Stack:{
    screen: MyStackNotAUth
  }
},{
  initialRouteName: "Stack",
  contentComponent: MainComponentDrawer,
})

const RootAppAuth = createMaterialTopTabNavigator({
  Home:{
    screen: MainComponent
  }
},{
  initialRouteName:'Home'
})



const SwitchStackAuth = createSwitchNavigator({
  AuthLoading: WelcomeComponent,
  App: RootAppAuth,
  NotAuthenticated: RootAppNotAuth,

},{
  initialRouteName: 'AuthLoading'
})

// rappitendero123321
export default class App extends React.Component {
  render() {
    return (
      <Provider store={ store }>
        <SwitchStackAuth screenProps={{ authUserName: '@elaine' }}/>
      </Provider>
    );
  }
}


